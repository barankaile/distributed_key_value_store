# README #

Dockerfile illustrates steps necessary to run program.
### What is this repository for? ###

Implementation of a Distributed, fault-tolerant, eventually-consistent key value store.

### How do I get set up? ###
test_HW4.py will run a series of tests on the system, tests include partitioning nodes and checking for balanced distribution of key value pairs

### Contribution guidelines ###
kbaran (Kaile Baran)
	 - Worked on node addition and removal, detecting and healing network partitions, key addition, and replication.

lhartlag (Lucas Hartlage) 
	- Worked on node addition and removal, node initialization, node/partition information handling, KVS debugging

dhalsey (Dustin Halsey)
	- Worked on the KVS system, key addition, key replication, and key redistribution on partition removal. + causal payload
